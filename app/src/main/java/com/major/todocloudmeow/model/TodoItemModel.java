package com.major.todocloudmeow.model;

/**
 * Created by Major on 7/4/2015.
 */
public class TodoItemModel {
    private String itemName;
    private boolean checked;

    public TodoItemModel(String itemName, boolean checked) {
        this.itemName = itemName;
        this.checked = checked;
    }

    private TodoItemModel() {
        //required for firebase
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setIsChecked(boolean isChecked) {
        this.checked = isChecked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TodoItemModel that = (TodoItemModel) o;

        if (checked != that.checked) return false;
        return itemName.equals(that.itemName);

    }

    @Override
    public int hashCode() {
        int result = itemName.hashCode();
        result = 31 * result + (checked ? 1 : 0);
        return result;
    }
}
