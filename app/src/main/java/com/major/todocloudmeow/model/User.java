package com.major.todocloudmeow.model;

/**
 * Created by l.major on 9/18/2015.
 */
public class User {
    String provider;
    String name;

    public User(String provider, String name) {
        this.provider = provider;
        this.name = name;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
