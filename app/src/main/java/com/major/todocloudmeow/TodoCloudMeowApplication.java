package com.major.todocloudmeow;

import android.app.Application;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import timber.log.Timber;

/**
 * Created by Major on 7/4/2015.
 */
public class TodoCloudMeowApplication extends Application {

    public Firebase getFirebaseRef() {
        return mFirebaseRef;
    }

    public void setFirebaseRef(Firebase firebaseRef) {
        this.mFirebaseRef = firebaseRef;
    }

    Firebase mFirebaseRef;
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);


        // other setup code
    }

}
