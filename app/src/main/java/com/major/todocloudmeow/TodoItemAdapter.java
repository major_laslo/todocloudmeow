package com.major.todocloudmeow;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.major.todocloudmeow.model.TodoItemModel;

import java.util.List;

/**
 * Created by Major on 7/4/2015.
 */
public class TodoItemAdapter extends RecyclerView.Adapter<TodoItemAdapter.ViewHolder> {

    public void setmTodoItemList(List<TodoItemModel> mTodoItemList) {
        this.mTodoItemList = mTodoItemList;
    }

    private List<TodoItemModel> mTodoItemList;

    public TodoItemAdapter(List<TodoItemModel> todoItemModelList) {
        mTodoItemList = todoItemModelList;
    }

    @Override
    public TodoItemAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        View rootView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.todo_item_list_layout, viewGroup, false);
        ViewHolder vh = new ViewHolder(rootView);
        return vh;
    }

    @Override
    public void onBindViewHolder(TodoItemAdapter.ViewHolder viewHolder, int position) {
            viewHolder.mTextView.setText(mTodoItemList.get(position).getItemName());
    }

    @Override
    public int getItemCount() {
        return mTodoItemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(View rootView) {
            super(rootView);
            mTextView = (TextView) rootView.findViewById(R.id.item_name_tv);
        }
    }
}
