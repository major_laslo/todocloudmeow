package com.major.todocloudmeow;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.major.todocloudmeow.interfaces.TodoItemRecyclerListener;
import com.major.todocloudmeow.model.TodoItemModel;

import java.util.List;

/**
 * Created by Major on 7/4/2015.
 */
public class TodoItemRecyclerAdapter extends RecyclerView.Adapter<TodoItemRecyclerAdapter.ViewHolder> implements TodoItemRecyclerListener {

    private static final String TAG = TodoItemRecyclerAdapter.class.getSimpleName();

    private Context mContext;

    public void setTodoItemList(List<TodoItemModel> mTodoItemList) {
        this.mTodoItemList = mTodoItemList;
    }

    public List<TodoItemModel> getTodoItemList() {
        return mTodoItemList;
    }

    private List<TodoItemModel> mTodoItemList;

    public TodoItemRecyclerAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public TodoItemRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        View rootView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.todo_item_list_layout, viewGroup, false);
        return new ViewHolder(rootView, this);
    }

    @Override
    public void onBindViewHolder(final TodoItemRecyclerAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.mTextView.setText(mTodoItemList.get(position).getItemName());
        if (mTodoItemList.get(position).isChecked()) {
            viewHolder.mTextView.setPaintFlags(viewHolder.mTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        viewHolder.mCheckBox.setChecked(mTodoItemList.get(position).isChecked());
        viewHolder.mCheckBox.setOnCheckedChangeListener(viewHolder);
    }

    @Override
    public int getItemCount() {
        return mTodoItemList.size();
    }

    @Override
    public void deleteIconClicked(int position) {
        mTodoItemList.remove(position);
        notifyItemRemoved(position);
        ((MainActivity)mContext).updateFireDb(mTodoItemList);
    }

    @Override
    public void checkedChanged(int position, boolean isChecked) {
        mTodoItemList.get(position).setIsChecked(isChecked);
        ((MainActivity) mContext).updateFireDb(mTodoItemList);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        // each data item is just a string in this case
        public TextView mTextView;
        public TodoItemRecyclerListener listener;
        public ImageView mDeleteIcon;
        public CheckBox mCheckBox;

        public ViewHolder(View rootView, final TodoItemRecyclerListener listener) {
            super(rootView);
            this.listener = listener;
            mTextView = (TextView) rootView.findViewById(R.id.item_name_tv);
            mDeleteIcon = (ImageView) rootView.findViewById(R.id.delete_iv);
            mCheckBox = (CheckBox) rootView.findViewById(R.id.item_cb);
            mDeleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.deleteIconClicked(getAdapterPosition());
                }
            });
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d(TAG, "onCheckedChanged:  " + isChecked + " at position: " + getAdapterPosition());
            listener.checkedChanged(getAdapterPosition(), isChecked);
            if (isChecked) {
                mTextView.setPaintFlags(mTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                mTextView.setPaintFlags(mTextView.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            }
        }
    }
}
