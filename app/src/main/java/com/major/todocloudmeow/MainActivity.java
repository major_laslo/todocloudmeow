package com.major.todocloudmeow;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.GenericTypeIndicator;
import com.firebase.client.ValueEventListener;
import com.major.todocloudmeow.model.TodoItemModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    Firebase mFirebaseRef;

    @Bind(R.id.root_layout)
    RelativeLayout mRootLayout;

    @Bind(R.id.message_et)
    EditText mMessageEt;

    @Bind(R.id.save_btn)
    Button mSaveBtn;

    @Bind(R.id.my_recycler_view)
    RecyclerView mTodoItemRecycler;

    TodoItemRecyclerAdapter mTodoItemAdapter;

    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUserId = getIntent().getStringExtra(Constants.USER_ID);

        ButterKnife.bind(this);
        mFirebaseRef = ((TodoCloudMeowApplication)getApplicationContext()).getFirebaseRef();


        mTodoItemRecycler.setLayoutManager(new LinearLayoutManager(this));
        mTodoItemAdapter = new TodoItemRecyclerAdapter(this);


        mFirebaseRef.child("lists").child(mUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.d(TAG, "onDataChange");

                GenericTypeIndicator<ArrayList<TodoItemModel>> genericTypeIndicator = new GenericTypeIndicator<ArrayList<TodoItemModel>>() {
                };
                List<TodoItemModel> todoList = snapshot.getValue(genericTypeIndicator);
                if (todoList == null) {
                    todoList = new ArrayList<>();
                }
                Iterator<TodoItemModel> iterator = todoList.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next() == null) {
                        iterator.remove();
                    }
                }
                if (mTodoItemAdapter.getTodoItemList() == null) {
                    Log.d(TAG, "adapter initialization");
                    mTodoItemAdapter.setTodoItemList(todoList);
                    mTodoItemRecycler.setAdapter(mTodoItemAdapter);
                }
                if (!mTodoItemAdapter.getTodoItemList().equals(todoList)) {
                    Log.d(TAG, "lists differ, update adapter");
                    mTodoItemAdapter.setTodoItemList(todoList);
                    mTodoItemAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });
    }

    @OnClick(R.id.save_btn)
    public void saveButtonClicked(View view) {
        Log.d(TAG, "saveButtonClicked)");
        String message = mMessageEt.getText().toString();
        Log.d(TAG, message);

        if (!"".equalsIgnoreCase(message)) {
            mTodoItemAdapter.getTodoItemList().add(new TodoItemModel(message, false));
            mTodoItemAdapter.notifyItemInserted(mTodoItemAdapter.getTodoItemList().size() - 1);
            mFirebaseRef.child("lists").child(mUserId).setValue(mTodoItemAdapter.getTodoItemList());
            Snackbar.make(view, "messages saved!", Snackbar.LENGTH_SHORT).show();
            mMessageEt.setText("");
        }
    }

    public void updateFireDb(List<TodoItemModel> todoItemModelList) {
        Log.d(TAG, "updateFireDb");
        mFirebaseRef.child("lists").child(mUserId).setValue(todoItemModelList);
        Snackbar.make(mRootLayout, "Database updated", Snackbar.LENGTH_SHORT).show();
    }

}
