package com.major.todocloudmeow;

/**
 * Created by l.major on 9/18/2015.
 */
public class Constants {

    public static final String USER_ID = "userId";

    public static final String FIRE_TOKEN = "fireToken";
}
