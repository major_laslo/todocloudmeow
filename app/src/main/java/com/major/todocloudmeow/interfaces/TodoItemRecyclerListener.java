package com.major.todocloudmeow.interfaces;

/**
 * Created by Major on 7/4/2015.
 */
public interface TodoItemRecyclerListener {
    void deleteIconClicked(int position);
    void checkedChanged(int position, boolean isChecked);
}
